// This file gives the information for the initial value and the (time-dependent)
// coupling in the Hamiltonian.
// This version is for the harmonic oscillator example.
//
// Copyright: Katharina Kormann, 2010-2017

#ifndef initial_ho_h
#define initial_ho_h

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <stdlib.h>

using namespace dealii;

// @sect3(Initial data and coefficients)

template <int dim>
class SolutionBase
{
protected:
  static const unsigned int n_source_centers = 3;
  static const Point<dim>   source_centers[n_source_centers];
  static const double       width;
};

template <>
const Point<1>
SolutionBase<1>::source_centers[SolutionBase<1>::n_source_centers]
  = { Point<1>(0.5)};

// Likewise, we can provide an explicit
// specialization for <code>dim=2</code>. We place the
// centers for the 2d case as follows:
template <>
const Point<2>
SolutionBase<2>::source_centers[SolutionBase<2>::n_source_centers]
  = { Point<2>(-1.0, -1.0)
      //,Point<2>(-0.5, -0.5),
      //Point<2>(+0.5, -0.5)
    };

template <>
const Point<3>
SolutionBase<3>::source_centers[SolutionBase<3>::n_source_centers]
  = { Point<3>(-1.0,-1.0,-1.0)};

template <int dim>
const double SolutionBase<dim>::width = 1.0;

template <int dim>
class Initial_Value : public Function<dim>,
  protected SolutionBase<dim>
{
public:
  Initial_Value () : Function<dim>() {}

  virtual double value (const Point<dim>   &p,
                        const unsigned int  component = 0) const;
};

template <int dim>
double Initial_Value<dim>::value (const Point<dim>   &p,
                                  const unsigned int) const
{
  double return_value = 0;

  //const Point<dim> x_minus_xi = p - this->source_centers[0];
  const Tensor<1,dim> x_minus_xi = p - this->source_centers[0];
  return_value = std::exp(-0.5*x_minus_xi.norm_square() /
                          (this->width * this->width))/
                 (std::pow(std::sqrt(std::sqrt(numbers::PI))*
                           this->width,
                           (double)dim));
// Comment on normalization (L2-norm = 1):
// f(x) = exp(-(x-x0)^2/(2*sigma^2)) *1/((2pi)^(dim/2)*sigma^(1/dim)) has L1-norm 1.
// The L2 Norm of f(x) = C*exp(-(x-x0)^2/(2*sigma^2)) is C times the root of the
// L1-norm of g(x)=exp(-(x-x0)^2/(sigma^2)).
// L1-norm of g(x) is  pi^(dim/2)*sigma^(1/dim)
// Hence, C should be sqrt(pi^(dim/2)*sigma^(1/dim)).
//-------------- Alternative Explanation:
// Factor two removed from usual normalization factor since normalization
// in L2-norm should be attained. Additional factor 1/2 for 1/2 of the integral
// in L2-norm.
  return return_value;
}

template <int dim>
class Potential : public Function<dim>,
  protected SolutionBase<dim>
{
public:
  Potential () : Function<dim>() {}

  virtual double value (const Point<dim>   &p,
                        const unsigned int  component = 0) const;
};

// The system matrix is given by the
// Hamiltonian. The potential energy is of the
// form coefficient*solution^2.
// The following function defines this
// coefficient.
template <int dim>
double Potential<dim>::value (const Point<dim>   &p,
                              const unsigned int component) const
{
  double return_value = 0;
  if (dim==1)
    return_value = p(0)*p(0)*0.5;
  if (dim==2)
    return_value = p(0)*p(0)*0.5+p(1)*p(1)*0.5;
  if (dim ==3)
    return_value = 0.5*(p(0)*p(0)+p(1)*p(1)+p(2)*p(2));
  if (component == 1)
    return_value += 1;
  return return_value;
}



//coupling function
class CouplingBase
{
protected:
  CouplingBase (const double coupling_strength)
    :
    e0(coupling_strength)
  {}

  static const double tau2fs;
  static const double fwhm;
  const double e0;
  static const double t0;
  static const int    lambda = 1000;
  static const double omega;
  static const double sig;
};

const double
CouplingBase::tau2fs = 0.024189;

const double
CouplingBase::fwhm  = 0.3;

const double
CouplingBase::t0 = 0.5;

const double
CouplingBase::omega  = 1;

const double
CouplingBase::sig  = 0.5;

class Coupling : protected CouplingBase
{
public:
  Coupling(const double coupling_strength)
    :
    CouplingBase(coupling_strength)
  {}

  double value (const double &time) const;
};


double
Coupling::value (const double &time) const
{
  double return_value = 0;
  return_value = this->e0*std::exp(-0.5*std::pow((time-this->t0)/this->sig,2))*
                 std::cos(this->omega*(time-this->t0));

  return return_value;
}

#endif
