// Example program solving an harmonic operator example of the multi-state
// time-dependent Schrödinger equation (without spacial adaptivity)
//
// Reference: Kormann, A time-space adaptive method for the Schr{\"o}dinger equation, Commun. Comput. Phys., 20:60--85, 2016.
//
// Copyright: Katharina Kormann, 2010-2017

// @sect1{Include files}

#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/parameter_handler.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/timer.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/trilinos_sparsity_pattern.h>
#include <deal.II/lac/trilinos_vector.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/trilinos_precondition.h>
#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/distributed/tria.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/tria_boundary_lib.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/fe_dgq.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/solution_transfer.h>
#include <deal.II/distributed/solution_transfer.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/base/smartpointer.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/base/convergence_table.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/lac/la_parallel_vector.h>
#include <deal.II/matrix_free/matrix_free.h>
#include <deal.II/matrix_free/fe_evaluation.h>

#include <typeinfo>
#include <fstream>
#include <iostream>



using namespace dealii;

// @sect2{Parameters for the simulation}

// Here, we set some parameters that can be changed
// to change the simulation.

struct SchrodingerParameters
{

  SchrodingerParameters(const std::string parameter_file)
  {
    ParameterHandler parameter_handler;
    parameter_handler.declare_entry ("Number of Gauss-Lobatto points", "7",
                                     Patterns::Integer (2, 10),
                                     "Convergence order of FEM.");
    parameter_handler.declare_entry ("Number of grid refinements", "2",
                                     Patterns::Integer (),
                                     "The number of times to perform global mesh refinement.");
    parameter_handler.declare_entry ("Final time", "10",
                                     Patterns::Double (),
                                     "Final time.");
    parameter_handler.declare_entry ("Maximum time step", "0.01",
                                     Patterns::Double (),
                                     "Maximally allowed time step.");
    parameter_handler.declare_entry ("Tolerance Magnus-Lanczos", "1e-5",
                                     Patterns::Double (),
                                     "Tolerance for adaptive mesh refinement.");

    parameter_handler.declare_entry ("Coupling strength", "1.0",
                                     Patterns::Double(),
                                     "Strength of coupling between parameters");

    parameter_handler.parse_input (parameter_file);

    gl_points = parameter_handler.get_integer("Number of Gauss-Lobatto points");
    glob_ref = parameter_handler.get_integer("Number of grid refinements");
    time_max = parameter_handler.get_double("Final time");
    delta_time_max = parameter_handler.get_double("Maximum time step");
    threshold = parameter_handler.get_double("Tolerance Magnus-Lanczos");
    coupling_strength = parameter_handler.get_double("Coupling strength");

    BLOCK_SIZE = 2;
    USE_PARTITION_PARTITION = false;
    USE_MULTITHREADING = false;

    mass = 1.0;
    tolerance = threshold / time_max;
    max_mult = 2000000;
  }

  // First part of parameters that can be controlled by parameter file.
  
  unsigned int gl_points; // Number of Gauss-Lobatto points per cell.

  unsigned int glob_ref;  // Number of global refinements of the cell.

  double time_max;        // Final time.

  double delta_time_max;  // Maximum time step

  double threshold;       // Error tolerance

  double coupling_strength; // Strength of two-state coupling
  
// The second part of parameters should not be changed for the harmonic oscillator example.
  
  // Some parameters to change parallelization.
  unsigned int BLOCK_SIZE;
  bool USE_PARTITION_PARTITION;
  bool USE_MULTITHREADING;

  double mass;   // Mass of the particle (normalized to one in HO example).
  double tolerance; // Tolerance for adaptive propagator need to be scaled by time_max.
  unsigned int max_mult; // Maximum number of MVP allowed in the simulation.

  static const unsigned int dimension = 2;    // Dimension of the problem (two for HO example).
  static const unsigned int no_states = 2;    // Number of states (two for HO example).
  static const unsigned int no_grids = 1;     // Number of different grids used in the simulation.

};



// Structure holding information for the Hamiltonian.

struct HamiltonianData
{
  std::vector<LinearAlgebra::distributed::Vector<double> > potential;
  LinearAlgebra::distributed::Vector<double>  system_lmm, system_lmm_inv,
                delta_potential;
  double pulse;
  double mass_factor;
};


// Include local

#include "matrix_free_gl_ho.h"
#include "initial_ho.h"
#include "expo_int.h"


// Main class for the Schrodinger problem.


template <int dim, int GL_POINTS>
class SchrodingerProblem
{
  typedef double number;
public:
  enum RefinementMode
  {
    global_refinement, adaptive_refinement // Adaptive refinement mode is disabled in this example program to keep it simple.
  };

  SchrodingerProblem (const SchrodingerParameters &parameters);

  ~SchrodingerProblem ();

  void run ();

private:
  void set_grid ();
  void setup_system (const bool grid_set=false,
                     const bool solution_set=false);
  void set_values_from_function();
  void update_time_step(double &delta_time,
                        const double &time,
                        const double max_time);
  void solve ();

  void print_vector_fe(const unsigned int step,
                       const std::vector<LinearAlgebra::distributed::Vector<number> >
                       &vector_print,
                       const std::string name);
  void initialize_cell_numbering ();

  const SchrodingerParameters             parameters;

  parallel::distributed::Triangulation<dim> triangulation;

  FE_Q<dim>                               fe;

  DoFHandler<dim>                         dof_handler;
  DoFHandler<dim>                         cell_handler;

  ConstraintMatrix                        hanging_node_constraints;

  MatrixFree<dim,number>                  system_matrix_mf_data;
  MatrixFreeIntegrator<dim,std::vector<LinearAlgebra::distributed::Vector<number> >,GL_POINTS, 0>
  system_matrix_mf;
  std::vector<LinearAlgebra::distributed::Vector<number> >           solution,
      system_rhs,
      vector;
  std::vector<HamiltonianData>             hamiltonian_data;

  ConvergenceTable                         convergence_table;
  std::vector<unsigned int>                n_q_points;
  const Coupling                           coupling;
  std::vector<number>                      times;
  IndexSet locally_relevant;
  IndexSet locally_owned_cells,ghost_cells;

  ConditionalOStream pcout;
};


// Constructor
template <int dim, int GL_POINTS>
SchrodingerProblem<dim,GL_POINTS>::SchrodingerProblem (const SchrodingerParameters &parameters) :
  parameters (parameters),
  triangulation(MPI_COMM_WORLD),
  fe (parameters.gl_points-1),
  dof_handler (triangulation),
  cell_handler(triangulation),
  system_matrix_mf_data(),
  system_matrix_mf(system_matrix_mf_data),
  coupling (parameters.coupling_strength),
  pcout (std::cout, Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)==0)
{}

// Destructor
template <int dim, int GL_POINTS>
SchrodingerProblem<dim,GL_POINTS>::~SchrodingerProblem ()
{
  dof_handler.clear ();
  cell_handler.clear();
}


// Set grid properties
template<int dim, int GL_POINTS>
void SchrodingerProblem<dim,GL_POINTS>::set_grid ()
{

#ifndef DEAL_II_WITH_P4EST
  GridTools::partition_triangulation (Utilities::System::
                                      get_n_mpi_processes(MPI_COMM_WORLD),
                                      triangulation);
#endif

  std::vector<IndexSet> locally_owned(2);
  {
    dof_handler.distribute_dofs (fe);
#ifndef DEAL_II_WITH_P4EST
    DoFRenumbering::subdomain_wise (dof_handler);
#endif

    const unsigned int n_active_cells=triangulation.n_active_cells();
    const unsigned int n_dofs=dof_handler.n_dofs();
    pcout << "   Number of active cells:       "
          << n_active_cells
          << std::endl
          << "   Number of degrees of freedom: "
          << n_dofs
          << std::endl;

    hanging_node_constraints.clear ();
    DoFTools::extract_locally_relevant_dofs (dof_handler,locally_relevant);
    hanging_node_constraints.reinit (locally_relevant);
    DoFTools::make_hanging_node_constraints (dof_handler,
                                             hanging_node_constraints);
    // Boundary conditions (homogeneous Dirichlet)
    VectorTools::interpolate_boundary_values (dof_handler,
                                              0,
                                              ZeroFunction<dim>(),
                                              hanging_node_constraints);
    hanging_node_constraints.close ();

  }
}

// Initialize the finite element operator and data structures for the TDSE solver.
template <int dim, int GL_POINTS>
void SchrodingerProblem<dim,GL_POINTS>::setup_system (const bool grid_set,
                                                      const bool solution_set)
{

  if (grid_set==false)
    {
      set_grid();
    }
  typename MatrixFree<dim,number>::AdditionalData
  parallelization_parameters;
  if (parameters.USE_MULTITHREADING == false)
    parallelization_parameters.tasks_parallel_scheme =
      MatrixFree<dim,number>::AdditionalData::none;
  else if (parameters.USE_PARTITION_PARTITION == true)
    parallelization_parameters.tasks_parallel_scheme =
      MatrixFree<dim,number>::AdditionalData::partition_partition;
  else
    parallelization_parameters.tasks_parallel_scheme =
      MatrixFree<dim,number>::AdditionalData::partition_color;
  parallelization_parameters.tasks_block_size = parameters.BLOCK_SIZE;
  parallelization_parameters.mapping_update_flags |= update_second_derivatives | update_quadrature_points;

  {
    QGaussLobatto<1> support (parameters.gl_points);
    system_matrix_mf_data.reinit(dof_handler, hanging_node_constraints,
                                 support, parallelization_parameters);
  }

  n_q_points.resize(1);
  n_q_points[0] = Utilities::fixed_power<dim>(parameters.gl_points);
  {
    hamiltonian_data.resize(1);


    for (unsigned int no=0; no<1; no++)
      {
        hamiltonian_data[no]. mass_factor = 1.0/(2.0*parameters.mass);
        hamiltonian_data[no].pulse = 0.0;
        system_matrix_mf_data.initialize_dof_vector(hamiltonian_data[no].system_lmm,no);
        hamiltonian_data[no].system_lmm_inv.reinit(hamiltonian_data[no].system_lmm);
        hamiltonian_data[no].delta_potential.reinit(hamiltonian_data[no].system_lmm);
        hamiltonian_data[no].potential.resize(parameters.no_states);
        for (unsigned int k=0; k<parameters.no_states; k++)
          hamiltonian_data[no].potential[k].reinit
          (hamiltonian_data[no].system_lmm);
      }

    {
      FEEvaluation<dim,GL_POINTS-1,GL_POINTS,1,double> fe_eval(system_matrix_mf_data,0,0);

      for (unsigned int cell=0; cell<system_matrix_mf_data.get_size_info().n_macro_cells; cell++)
        {
          fe_eval.reinit(cell);
          for (unsigned int q=0; q<n_q_points[0]; q++)
            {
              fe_eval.submit_value(make_vectorized_array<double>(1.),q);
            }
          fe_eval.integrate(true,false);
          fe_eval.distribute_local_to_global(hamiltonian_data[0].system_lmm);
        }

    }


    for (unsigned int no=0; no<1; no++)
      {
        hamiltonian_data[no].system_lmm.compress(VectorOperation::add);

        for (unsigned int k=0; k<hamiltonian_data[no].system_lmm.local_size(); k++)
          if (hamiltonian_data[no].system_lmm.local_element(k)>1e-15)
            hamiltonian_data[no].system_lmm_inv.local_element(k) =
              1./hamiltonian_data[no].system_lmm.local_element(k);
          else
            hamiltonian_data[no].system_lmm_inv.local_element(k) = 0;
        hamiltonian_data[no].system_lmm.update_ghost_values();
        hamiltonian_data[no].system_lmm_inv.update_ghost_values();
      }
  }

  if (solution_set == false)
    {
      solution.resize(2*parameters.no_states);
      system_rhs.resize(2*parameters.no_states);
      system_matrix_mf_data.initialize_dof_vector(solution[0]);
    }
  vector.resize(2*parameters.no_states);
  for (unsigned int j=0; j<2*parameters.no_states; j++)
    {
      if (j>0 && solution_set == false)
        {
          solution[j].reinit (solution[0]);
        }
      vector[j].reinit(solution[0]);
      system_rhs[j].reinit (solution[0]);
    }

  system_matrix_mf_data.print_memory_consumption(pcout);
  pcout << "Memory consumption vectors: ";
  std::size_t memory = MemoryConsumption::memory_consumption(solution);
  memory +=  MemoryConsumption::memory_consumption(vector);
  memory +=  MemoryConsumption::memory_consumption(system_rhs);
  pcout << 1e-6 * memory << " MB" << std::endl;

  set_values_from_function();
  if (solution_set == false)
    {
      solution[0] = system_rhs[0];
    }

  system_matrix_mf.reinit(hamiltonian_data[0]);
  pcout << "Vector memory consumption: " << solution[0].memory_consumption()
        << std::endl;
}


// Set values of the finite element degree of freedom from a given function
// Set the data for the Hamiltonian
template <int dim, int GL_POINTS>
void SchrodingerProblem<dim,GL_POINTS>::set_values_from_function ()
{
  Quadrature<dim> quadrature_formula (fe.get_unit_support_points());
  FEValues<dim> fe_values (fe, quadrature_formula, update_quadrature_points
                           | update_JxW_values);
  std::vector<double> sol_values (n_q_points[0]);
  const Initial_Value<dim> initial_value;

  Potential<dim> pot_value;
  std::vector<std::vector<double> > pot_values(parameters.no_states);
  for (unsigned int j=0; j<parameters.no_states; j++)
    {
      pot_values[j].resize(system_matrix_mf_data.get_dofs_per_cell(0));
    }

  std::vector<unsigned int> local_dof_indices (n_q_points[0]);
  typename DoFHandler<dim>::active_cell_iterator
  cell = dof_handler.begin_active();
  for (; cell!=dof_handler.end(); cell++)
    if (cell->subdomain_id() == Utilities::MPI::this_mpi_process(MPI_COMM_WORLD))
      {
        fe_values.reinit(cell);
        cell->get_dof_indices(local_dof_indices);
        initial_value.value_list(fe_values.get_quadrature_points(), sol_values);
        for (unsigned int k=0; k<parameters.no_states; k++)
          pot_value.value_list(fe_values.get_quadrature_points(),pot_values[k],k);
        for (unsigned int j=0; j<n_q_points[0]; j++)
          {
            if (dof_handler.locally_owned_dofs().is_element(local_dof_indices[j]))// Set to  0, when ever hanging_node_constraints.is_constrained(local_dof_indices[j])
              {
                if (hanging_node_constraints.is_constrained(local_dof_indices[j]))
                  {
                    system_rhs[0](local_dof_indices[j]) = 0;
                    for (unsigned int k=0; k<parameters.no_states; k++)
                      hamiltonian_data[0].potential[k](local_dof_indices[j]) = 0;
                  }
                else
                  {
                    system_rhs[0](local_dof_indices[j]) = sol_values[j];
                    for (unsigned int k=0; k<parameters.no_states; k++)
                      hamiltonian_data[0].potential[k](local_dof_indices[j]) =
                        pot_values[k][j];
                  }
              }
          }
      }
  hamiltonian_data[0].delta_potential =  hamiltonian_data[0].potential[1];
  hamiltonian_data[0].delta_potential -= hamiltonian_data[0].potential[0];
}


// Update time step to according to error in Magnus expansion (adaptive time step control)
template<int dim, int GL_POINTS>
void SchrodingerProblem<dim,GL_POINTS>::update_time_step(double &delta_time,
                                                         const double &time,
                                                         const double max_time)
{
  double remainder = system_matrix_mf.magnus2_norm(solution);
  remainder *= delta_time*
               std::abs(-coupling.value(time + delta_time/2.0 * (1.0-1.0/sqrt(3.0))) +
                        coupling.value(time + delta_time/2.0 * (1.0+1.0/sqrt(3.0))))/
               (4*sqrt(3));
  delta_time = std::min(std::max(std::min(delta_time*
                                          std::pow(0.9*parameters.tolerance/
                                                   remainder,1.0/2.0),
                                          5.0*delta_time),
                                 0.01*delta_time),max_time-time);
  delta_time = std::min(delta_time, parameters.delta_time_max);
}



//------
// Core routine that solves the TDSE
template <int dim, int GL_POINTS>
void SchrodingerProblem<dim,GL_POINTS>::solve ()
{
  const int n_mults = parameters.max_mult;
  Timer cputime;
  cputime.start();
  Vector<double> auto_real(n_mults+1),delta_time_vec(n_mults),p_vec(n_mults);
  auto_real(0) = system_matrix_mf.inner_product_real_mass(system_rhs,solution);

  int info = 0,n_lanczos=0;
  double delta_time = 0.01, time = 0;
  ExpoInt propagator;
  int j=0;

  for (; (time<times[parameters.no_grids])&&(j<n_mults); j++)
    {
      delta_time_vec(j) = delta_time;
      // Update pulse for current time
      hamiltonian_data[0].pulse = (coupling.value(time + delta_time/2.0 * (1.0-1.0/sqrt(3.0))) +
                                   coupling.value(time + delta_time/2.0 * (1.0+1.0/sqrt(3.0))))/2.0;
      // Call exponential integrator
      info = propagator.lanczos_p_adaptive(system_matrix_mf, 20,
                                           parameters.tolerance, delta_time,
                                           solution);
      if (info == -10)
        {
          delta_time /= 2;
          pcout << "Time step too large" << std::endl;
          --j;
          continue;
        }
      else
        n_lanczos += info;
      auto_real(j+1) = system_matrix_mf.inner_product_real_mass(system_rhs,solution);
      p_vec(j) = info;
      // Update current time of simulation
      time += delta_time;
      // Compute adaptive time step for next iteration
      update_time_step(delta_time,time,times[parameters.no_grids]);

      // Every once in a while print some info.
      if ((j%50)==0)
        {
          pcout << "Time: " << time << "  " << delta_time
                << "  " << hamiltonian_data[0].pulse << std::endl;
          print_vector_fe(j/50, solution, "output/solution");
        }
    }

  if (time<times[parameters.no_grids])
    pcout << "Stopped since maximal no. of time steps exceeded. Time = "
          << time << std::endl;
  // Output auto correlation, time step and size of Krylov subspace for current step.
  if (Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)==0)
    {
      auto_real.reinit(j+1,true);
      std::string filename = "output/auto_real-00.mat";
      {
        std::ofstream output(filename.c_str());
        auto_real.print(output,10,true,true);
      }
      delta_time_vec.reinit(j,true);
      filename = "output/delta_time-00.mat";
      {
        std::ofstream  output(filename.c_str());
        delta_time_vec.print(output,10,true,true);
      }
      p_vec.reinit(j,true);
      filename = "output/size_Krylov-00.mat";
      {
        std::ofstream  output(filename.c_str());
        p_vec.print(output,10,true,true);
      }
    }

  // Summary of simulation
  pcout << "Number of MVPs: " << n_lanczos << " Number of Time steps: "
        << j << " Average Krylov size: " << n_lanczos/j << std::endl ;
  pcout << "Time for " << n_lanczos << " MatVecs with matrix-free: "
        << cputime.wall_time() << "s" << std::endl;

}


// Output the solution
template <int dim, int GL_POINTS>
void SchrodingerProblem<dim,GL_POINTS>
::print_vector_fe (const unsigned int step,
                   const std::vector<LinearAlgebra::distributed::Vector<number> >
                   &vector_print,
                   const std::string name)
{
  //for (unsigned int ks=0; ks<2*parameters.no_states; ks++)
    {
      unsigned int ks=0;
      {
        const std::string filename = name + "1_real_" +
                                     Utilities::int_to_string(step,2)+
                                     Utilities::int_to_string(Utilities::MPI::
                                                              this_mpi_process(MPI_COMM_WORLD),2)+".mat";
        std::ofstream output(filename.c_str());
        vector_print[ks].print(output,5, true, true);
      }
      {
        DataOut<dim> data_out;
        data_out.attach_dof_handler(dof_handler);
        vector_print[ks].update_ghost_values();
        data_out.add_data_vector(vector_print[ks], "solution");
        data_out.build_patches(fe.degree);
        const std::string filename = name + "1_real_" +
                                     Utilities::int_to_string(step,2)+
                                     Utilities::int_to_string(Utilities::MPI::
                                                              this_mpi_process(MPI_COMM_WORLD),2)+".vtk";
        std::ofstream output(filename.c_str());
        data_out.write_vtk(output);
      }
    }
    {
      unsigned int ks=1;
      {
        const std::string filename = name + "1_imag_" +
                                     Utilities::int_to_string(step,2)+
                                     Utilities::int_to_string(Utilities::MPI::
                                                              this_mpi_process(MPI_COMM_WORLD),2)+".mat";
        std::ofstream output(filename.c_str());
        vector_print[ks].print(output,5, true, true);
      }
      {
        DataOut<dim> data_out;
        data_out.attach_dof_handler(dof_handler);
        vector_print[ks].update_ghost_values();
        data_out.add_data_vector(vector_print[ks], "solution");
        data_out.build_patches(fe.degree);
        const std::string filename = name + "1_imag_" +
                                     Utilities::int_to_string(step,2)+
                                     Utilities::int_to_string(Utilities::MPI::
                                                              this_mpi_process(MPI_COMM_WORLD),2)+".vtk";
        std::ofstream output(filename.c_str());
        data_out.write_vtk(output);
      }
    }
}


// Set up the system and solve.
template <int dim, int GL_POINTS>
void SchrodingerProblem<dim,GL_POINTS>::run ()
{
  // Set up triangulation
  GridGenerator::subdivided_hyper_cube(triangulation, 2 , -10, 10);
  triangulation.refine_global(parameters.glob_ref);

  // Set the time intervals for multiple adaptive grid (only one grid here for simplicity)
  times.resize(parameters.no_grids+1);
  times[parameters.no_grids] = parameters.time_max;
  pcout << "Setup system." << std::endl;
  setup_system (false,false);

  pcout << "Solve." << std::endl;
  solve ();
}


// Main program call.
int main (int argc, char **argv)
{

  try
    {
      Utilities::MPI::MPI_InitFinalize mpi_initialization(argc, argv, 1);
      deallog.depth_console (0);


      {
        ConditionalOStream pcout(std::cout, Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)==0);
        pcout << "Solving the Schrodinger equation (HO example)" << std::endl
              << "=============================================" << std::endl
              << std::endl;
        std::string paramfile = argc>1 ? argv[1] : "schrodinger_ho.prm";
        pcout << "Using parameters from file " << paramfile << std::endl;
        SchrodingerParameters parameters(paramfile);

        // Implement order 1 to 10
        if (parameters.gl_points == 2)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,2>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 3)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,3>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 4)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,4>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 5)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,5>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 6)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,6>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 7)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,7>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 8)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,8>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 9)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,9>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        if (parameters.gl_points == 10)
          {
            SchrodingerProblem<SchrodingerParameters::dimension,10>
            helmholtz_problem_2d (parameters);
            helmholtz_problem_2d.run ();
          }
        else
          AssertThrow(parameters.gl_points < 11,
                      ExcMessage("Only up to 10 GL points implemented"));

        pcout << std::endl;
      }
    }
  catch (std::exception &exc)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Exception on processing: " << std::endl
                << exc.what() << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }
  catch (...)
    {
      std::cerr << std::endl << std::endl
                << "----------------------------------------------------"
                << std::endl;
      std::cerr << "Unknown exception!" << std::endl
                << "Aborting!" << std::endl
                << "----------------------------------------------------"
                << std::endl;
      return 1;
    }

  return 0;
}


// What comes here is basically just
// an annoyance that you can ignore
// if you are not working on an AIX
// system: on this system, static
// member variables are not
// instantiated automatically when
// their enclosing class is
// instantiated. This leads to linker
// errors if these variables are not
// explicitly instantiated. As said,
// this is, strictly C++ standards
// speaking, not necessary, but it
// doesn't hurt either on other
// systems, and since it is necessary
// to get things running on AIX, why
// not do it:
template const double SolutionBase<2>::width;
template const double SolutionBase<3>::width;
