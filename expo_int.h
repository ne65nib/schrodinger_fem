// Exponential integrator program with adaptive control of the temporal error
// The temporal adaptivity is described in the paper:
//
// Kormann, Holmgren, Karlsson,  Global error control of the time-propagation
// for the Schrödinger equation with a time-dependent Hamiltonian, Journal of
// Computational Science 2, 2011
//
// Copyright: Katharina Kormann, 2010-2017


#ifndef __deal2__expo_int_h
#define __deal2__expo_int_h

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/trilinos_vector.h>
#include <typeinfo>
#include <fstream>
#include <iostream>
#include <complex>
#include <deal.II/lac/lapack_templates.h>
#include <new>
#include <stdlib.h>


// Lapack routine
extern "C" {
  void dsteqr_ (const char *compz, const int *n,
                double *d, double *e, double *z,
                const int *lzd, double *work,
                int *info);
}

DEAL_II_NAMESPACE_OPEN

// exponential integrator class
class ExpoInt
{
public:
  ExpoInt ();

  template <class MatrixType, typename VectorType>
  int lanczos_p_adaptive(const MatrixType &system_matrix,
                         const int iter,
                         const double tolerance,
                         const double delta_time,
                         std::vector<VectorType > &solution);


};


ExpoInt::ExpoInt ()
{}

// Implementation of Lanczos algorithm with adaptive size of Krylov space.
template <class MatrixType, typename VectorType>
int ExpoInt::lanczos_p_adaptive (const MatrixType &system_matrix,
                                 const int iter,
                                 const double tolerance,
                                 const double delta_time,
                                 std::vector<VectorType > &solution)

{
  int info_return = -10;
  std::vector<double> alpha(iter);
  std::vector<double> beta(iter-1);

  std::vector <VectorType> temp(solution.size());
  double norm_solution;
  std::vector <std::vector<VectorType > > krylov_basis_vectors(iter);

  for (unsigned int k=0; k<solution.size(); k++)
    {
      temp[k].reinit(solution[k]);
    }
  norm_solution = system_matrix.inner_product_real_mass(solution,solution);
  norm_solution = std::sqrt(norm_solution);

  krylov_basis_vectors[0] = solution;
  for (unsigned int k=0; k<solution.size(); k++)
    {
      krylov_basis_vectors[0][k] = solution[k];
      krylov_basis_vectors[0][k] /= norm_solution;
    }

  for (int i = 0; i < iter; i++)
    {
      if (i == 0)
        system_matrix.vmult(temp,krylov_basis_vectors[i]);
      else
        {
          krylov_basis_vectors[i] = temp;
          for (unsigned int k=0; k<solution.size(); k++)
            {
              krylov_basis_vectors[i][k] = temp[k];
              krylov_basis_vectors[i][k] /= beta[i-1];
            }
          system_matrix.vmult(temp,krylov_basis_vectors[i]);

          for (unsigned int k=0; k<solution.size(); k++)
            {
              temp[k].add(-beta[i-1],krylov_basis_vectors[i-1][k]);
            }
        }
      alpha[i] = system_matrix.inner_product_real_mass
                 (temp,krylov_basis_vectors[i]);
      for (unsigned int k=0; k<solution.size(); k++)
        {
          temp[k].add(-alpha[i],krylov_basis_vectors[i][k]);
        }
      if (i != iter-1)
        {
          beta[i] = system_matrix.inner_product_real_mass(temp,temp);
          beta[i] = std::sqrt(beta[i]);
        }

      {
        int info = 0;
        // Solve the (iter x iter) eigenvalue Problem
        const char compz = 'I';
        std::vector<double> work(2*i);
        int size_now = i+1;
        std::vector<double> eigen_vec_lan(size_now*size_now);
        std::vector<double> alpha_copy(size_now);
        std::vector<double> beta_copy(i);
        for (int j =0; j<(i+1)*(i+1); j++)
          eigen_vec_lan[j]=0;
        for (int j=0; j<(i+1); j++)
          {
            eigen_vec_lan[j*(i+1)]=1;
            alpha_copy[j] = alpha[j];
            if (j != size_now-1)
              beta_copy[j] = beta[j];
          }
        dsteqr_ (&compz, &size_now, alpha_copy.data(), beta_copy.data(), eigen_vec_lan.data(),
                 &size_now,work.data(),&info);

        double residual = 0;
        double residual_re =0;
        double residual_im = 0;

        for (int j=0; j<size_now; j++)
          {
            residual_re += eigen_vec_lan[i+j*(i+1)]*eigen_vec_lan[j*(i+1)]
                           *std::cos(-delta_time*alpha_copy[j]);
            residual_im += eigen_vec_lan[i+j*(i+1)]*eigen_vec_lan[j*(i+1)]
                           *std::sin(-delta_time*alpha_copy[j]);
          }
        residual = std::abs(beta[i]*
                            std::sqrt(residual_re*residual_re+
                                      residual_im*residual_im));
        if (residual < tolerance && info == 0 && i>2)
          {
            int actual_iter = i+1;
            info_return = actual_iter;

            std::vector<double> alpha1(actual_iter);
            std::vector<double> beta1(actual_iter);
            std::vector<double> beta2(actual_iter);

            for (int j=0; j<actual_iter; j++)
              {
                alpha_copy[j]= -delta_time*alpha_copy[j];
                beta2[j] = std::sin(alpha_copy[j])*eigen_vec_lan[j*actual_iter];
                alpha_copy[j] = std::cos(alpha_copy[j])*eigen_vec_lan[j*actual_iter];
              }


            //v*V1*expm(-i*dt*D)*V1'*v'*psi
            for (int j=0; j<actual_iter; j++)
              {
                alpha1[j]=0;
                for (int k=0; k<actual_iter; k++)
                  {
                    alpha1[j] += eigen_vec_lan[j+k*actual_iter]*alpha_copy[k];
                  }
              }
            for (int j=0; j<actual_iter; j++)
              {
                beta1[j] = 0;
                for (int k=0; k<actual_iter; k++)
                  {
                    beta1[j] += eigen_vec_lan[j+k*actual_iter]*beta2[k];
                  }
              }
            for (unsigned int k=0; k<solution.size(); k++)
              solution[k] = 0;

            for (unsigned int k=0; k<solution.size()/2; k++)
              {
                for (int i=0; i<actual_iter; i++)
                  {
                    solution[2*k].add(alpha1[i], krylov_basis_vectors[i][2*k],
                                      -beta1[i],krylov_basis_vectors[i][2*k+1]);
                    solution[2*k+1].add(alpha1[i],
                                        krylov_basis_vectors[i][2*k+1],
                                        beta1[i],krylov_basis_vectors[i][2*k]);
                  }
              }

            for (unsigned int l=0; l<solution.size(); l++)
              solution[l] *= norm_solution;

            break;
          }
      }
    }

  return info_return;
}


DEAL_II_NAMESPACE_CLOSE

#endif
