# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 11:50:22 2017

@author: Katharina Kormann
"""

# Diagnostics for FEM solver for the time-dependent Schrödinger equation

import numpy as np
import matplotlib.pyplot as plt


c=np.loadtxt('../output/delta_time-00.mat')
a=np.loadtxt('../output/auto_real-00.mat')

# Construct the time vector from the values of dt
ind=np.shape(c)
t = np.zeros(ind[0]+1);
for j in range(0,ind[0]):
    t[j+1] = t[j]+c[j]
    
plt.plot(t,a)
plt.xlabel('time')
plt.ylabel('real part of autocorrelation')
plt.savefig('auto.eps')
