// Implementation of the matrix-free operator for multi-state time-dependent
// Schrödinger equation
//
// Copyright: Katharina Kormann, 2010-2017



#ifndef matrix_free_gl_h
#define matrix_free_gl_h

#include "deal.II/matrix_free/matrix_free.h"
#include "deal.II/matrix_free/fe_evaluation.h"

using namespace dealii;


// Multiplication with Hamiltonian of multi-state system.
template<typename VectorType>
class MultWork
{
public:
  MultWork(VectorType &dst_in,
           const VectorType &src_in,
           const HamiltonianData &hamiltonian_data_in)
    :
    dst(dst_in),
    src(src_in),
    hamiltonian_data(hamiltonian_data_in)
  {};
#ifdef DEAL_II_WITH_THREADS
  void operator() (const tbb::blocked_range<unsigned int> &r) const
  {
    const unsigned int no_states = dst.size()/2;
    for (unsigned int j= r.begin(); j<r.end(); ++j)
      for (unsigned int k = 0; k<dst.size(); ++k)
        dst[k].local_element(j) = dst[k].local_element(j)*
                                  hamiltonian_data.mass_factor*
                                  hamiltonian_data.system_lmm_inv.local_element(j)+
                                  src[k].local_element(j)*
                                  hamiltonian_data.potential[k/2].local_element(j)
                                  +src[(k+no_states)%(2*no_states)].local_element(j)*
                                  hamiltonian_data.pulse;
  }
#else
  void operator() () const
  {
    const unsigned int no_states = dst.size()/2;
    const unsigned int size = dst(0).size();
    for (unsigned int j=0; j<size; ++j)
      for (unsigned int k = 0; k<dst.size(); ++k)
        dst[k].local_element(j) = dst[k].local_element(j)
                                  *hamiltonian_data.mass_factor*
                                  hamiltonian_data.system_lmm_inv.local_element(j)+
                                  src[k].local_element(j)*
                                  hamiltonian_data.potential[k/2].local_element(j)
                                  +src[(k+no_states)%(2*no_states)].local_element(j)*
                                  hamiltonian_data.pulse;
  }
#endif
private:
  VectorType &dst;
  const VectorType &src;
  const HamiltonianData &hamiltonian_data;
};

template<typename VectorType>
class WeightedInner
{
public:
  double sum;
#ifdef DEAL_II_WITH_THREADS
  void operator ()(const tbb::blocked_range<unsigned int> &r)
  {
    for (unsigned int j=r.begin(); j<r.end(); j++)
      for (unsigned int k=0; k<src1.size(); k++)
        sum += src1[k].local_element(j)*weights.local_element(j)
               *src2[k].local_element(j);
  }
#else
  void operator ()()
  {
    const unsigned int size = src1(0).size();
    for (unsigned int j=0; j<size; j++)
      for (unsigned int k=0; k<src.size(); k++)
        sum += src1[k].local_element(j)*weights.local_element(j)*
               src2[k].local_element(j);
  }
#endif

  WeightedInner(WeightedInner &x
#ifdef DEAL_II_WITH_THREADS
                , tbb::split
#endif
               ):
    sum(0),
    src1(x.src1),
    src2(x.src2),
    weights(x.weights)
  {};
  void join (const WeightedInner &x )
  {
    sum += x.sum;
  }
  WeightedInner (const VectorType &src1_in, const VectorType &src2_in,
                 const LinearAlgebra::distributed::Vector<double>  &weights_in):
    sum(0),
    src1(src1_in),
    src2(src2_in),
    weights(weights_in)
  {};
private:
  const VectorType &src1;
  const VectorType &src2;
  const LinearAlgebra::distributed::Vector<double> &weights;
};


// The MatrixFreeIntegrator gives a description of the matrix operations in
// matrix-free format.

// The class matrix free integrator is needed for computing inner products
// (based on the mass matrix) used for the Lanczos algorithms.
// Specific routines for error estimation are also provided.

template <int dim, typename VectorType, int n_dofs_1d, int type>
class MatrixFreeIntegrator
{
public:
  typedef typename DoFHandler<dim>::active_cell_iterator CellIterator;
  typedef typename VectorType::value_type::value_type Number;
  typedef VectorizedArray<Number> vector_t;

  MatrixFreeIntegrator(const dealii::MatrixFree<dim,Number> &data_in):
    data(data_in)
  {};
  void reinit(HamiltonianData &hamiltonian_in);
  void operator () (const dealii::MatrixFree<dim,Number> &data,
                    VectorType          &dst,
                    const VectorType    &src,
                    const std::pair<unsigned int,unsigned int> &cell_range) const;
  double inner_product_real_mass(const VectorType &src1,
                                 const VectorType &src2) const;
  //double population (const VectorType &src,
  //         const unsigned int state) const;
  double magnus2_norm (const VectorType &src) const;

  void vmult (VectorType &dst,
              const VectorType &src) const
  {
    for (unsigned int k=0; k<dst.size(); k++)
      dst[k] = 0;
    data.cell_loop (&MatrixFreeIntegrator::operator(),this,dst, src);
#ifdef DEAL_II_WITH_THREADS
    parallel_for(tbb::blocked_range<unsigned int>(0,dst[0].local_size(),
                                                  dst[0].local_size()/32),
                 MultWork<VectorType>(dst,src,*hamiltonian_data));
#else
    MultWork<VectorType>mult_work(dst,src,*hamiltonian_data);
    mult_work();
#endif
  }

private:
  HamiltonianData *hamiltonian_data;
  AlignedVector<vector_t> coefficient_z;
  const dealii::MatrixFree<dim,Number> &data;
};


template <int dim, typename VectorType, int n_dofs_1d, int type>
void MatrixFreeIntegrator<dim,VectorType,n_dofs_1d,type>::
reinit(HamiltonianData &hamiltonian_in)
{
  hamiltonian_data = &hamiltonian_in;

}

// Matrix-free Laplacian
template <int dim, typename VectorType, int n_dofs_1d,int type>
void MatrixFreeIntegrator<dim,VectorType,n_dofs_1d,type>::
operator () (const dealii::MatrixFree<dim,Number> &,
             VectorType &dst,
             const VectorType &src,
             const std::pair<unsigned int,unsigned int> &cell_range) const
{
  typedef VectorizedArray<Number> vector_t;
  typedef FEEvaluation<dim,n_dofs_1d-1,n_dofs_1d,1,Number> FEEvalType;
  FEEvalType fe_eval (this->data,type,type);
  for (unsigned int cell=cell_range.first; cell<cell_range.second; ++cell)
    {
      fe_eval.reinit (cell);
      for (unsigned int state=0; state<src.size(); ++state)
        {
          fe_eval.read_dof_values(src[state]);
          fe_eval.evaluate(false,true,false);
          for (unsigned int q=0; q<fe_eval.n_q_points; ++q)
            fe_eval.submit_gradient (fe_eval.get_gradient(q),q);
          fe_eval.integrate(false,true);
          fe_eval.distribute_local_to_global(dst[state]);
        }
    }

}


// Inner product induced by mass matrix (real part)
template <int dim, typename VectorType, int n_dofs_1d,int type>
double MatrixFreeIntegrator<dim,VectorType,n_dofs_1d,type>::
inner_product_real_mass (const VectorType &src1,
                         const VectorType &src2) const
{
  AssertDimension(src1[0].local_size(), src2[0].local_size());
  AssertDimension(src1[0].local_size(), hamiltonian_data->system_lmm.local_size());
  WeightedInner<VectorType> mi(src1,src2,hamiltonian_data->system_lmm);
#ifdef DEAL_II_WITH_THREADS
  parallel_reduce(tbb::blocked_range<unsigned int>
                  (0,src1[0].local_size(),src1[0].local_size()/32), mi);
#else
  mi();
#endif
  return Utilities::MPI::sum(mi.sum, MPI_COMM_WORLD);
}


// Norm computation for error estimate in Magnus expansion of order two.
template <int dim, typename VectorType, int n_dofs_1d,int type>
double MatrixFreeIntegrator<dim,VectorType,n_dofs_1d,type>::
magnus2_norm (const VectorType &src) const
{
  WeightedInner<VectorType> wi(src,src,hamiltonian_data->delta_potential);
#ifdef DEAL_II_WITH_THREADS
  parallel_reduce(tbb::blocked_range<unsigned int>
                  (0,src[0].local_size(),src[0].local_size()/32), wi);
#else
  wi();
#endif
  return std::sqrt(Utilities::MPI::sum(wi.sum, MPI_COMM_WORLD));
}


#endif
